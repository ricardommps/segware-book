import React from 'react';
import { Route, Switch } from 'react-router';
import { IntlProvider } from 'react-intl';
import { useSelector } from 'react-redux';
import { messages, formats } from './i18n';
import Auth from 'containers/Auth';
import Shell from 'containers/Shell';
import Home from 'containers/Home';
import Feed from 'containers/DialogFeed';

const App = () => {
  const auth = useSelector(state => state.auth);
  const language = navigator.language;

  return (
    <IntlProvider timeZone="utc" locale={language} messages={messages} formats={formats}>
      {!auth.token ? (
        <Auth />
      ) : (
        <Switch>
          <Route path="/">
            <Shell>
              <Home />
              <Route path="/feed/">
                <Feed />
              </Route>
            </Shell>
          </Route>
        </Switch>
      )}
    </IntlProvider>
  );
};

export default App;
