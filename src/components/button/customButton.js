import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

export const DefaultButton = withStyles(theme => ({
  root: {
    color: '#ef3340',
    backgroundColor: 'transparent',
    borderRadius: '4px',
    textDecoration: 'none',
    padding: '8px 20px',
    border: '1px solid #ef3340',
    '&:hover': {
      backgroundColor: '#ef3340',
      color: '#000',
    },
    '&:disabled': {
      border: 'none',
      padding: '0px',
    },
  },
}))(Button);
