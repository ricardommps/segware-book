import React from 'react';
import { FormattedDate, useIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Badge from '@material-ui/core/Badge';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 345,
  },
  avatar: {
    backgroundColor: '#f44336',
  },
  icon: {
    margin: '10px',
  },
}));

const CardFeed = ({ feed, handleReaction }) => {
  const i18n = useIntl();
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Tooltip title={feed.author?.username}>
            <Avatar aria-label="recipe" className={classes.avatar}>
              {feed.author?.username[0].toUpperCase()}
            </Avatar>
          </Tooltip>
        }
        subheader={
          <FormattedDate year="numeric" month="2-digit" day="2-digit" value={feed.createdAt} />
        }
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {feed.content}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Grid container spacing={2}>
          <Grid item xs={2}>
            <Badge badgeContent={feed.likes} color="error">
              <ThumbUpIcon />
            </Badge>
          </Grid>
          <Grid item xs={2}>
            <Badge badgeContent={feed.loves} color="primary">
              <FavoriteIcon />
            </Badge>
          </Grid>
          <Grid container item xs={12}>
            <Grid item xs={5} sm={4}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                startIcon={<ThumbUpIcon />}
                onClick={() => handleReaction(feed.id, true, false)}
              >
                {i18n.formatMessage({ id: 'home.card.button.like' })}
              </Button>
            </Grid>
            <Grid item xs={5} sm={4}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                startIcon={<FavoriteIcon color="error" />}
                onClick={() => handleReaction(feed.id, false, true)}
              >
                {i18n.formatMessage({ id: 'home.card.button.love' })}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  );
};

export default CardFeed;
