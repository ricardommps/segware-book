import React, { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import styled from 'styled-components';
import { DefaultButton } from 'components/button/customButton';
import Alert from 'components/Alert/Alert';

const Title = styled.div`
  padding: 23px;
  font-size: 18px;
`;

const WrapperButton = styled.div`
  padding-top: 1rem;
`;

const useStyles = makeStyles(theme => ({
  textField: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingBottom: 0,
    marginTop: 0,
    fontWeight: 500,
  },
  input: {
    color: 'white',
  },
}));

const AuthForm = ({
  type,
  setType,
  state,
  setState,
  handlerSubmit,
  loginError,
  registerError,
  registerSuccess,
}) => {
  const i18n = useIntl();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [msgError, setMsgError] = useState('auth.login.error');

  useEffect(() => {
    if (loginError) {
      setOpen(true);
      setMsgError('auth.login.error');
    }
  }, [loginError]);

  useEffect(() => {
    if (registerError) {
      setOpen(true);
      setMsgError('auth.register.error');
    }
  }, [registerError]);

  useEffect(() => {
    if (registerSuccess) {
      setOpen(true);
      setMsgError('auth.register.succes');
      setType('signIn');
    }
  }, [registerSuccess, setType]);

  const handlerChangeUsername = value => {
    setState(prevState => ({
      ...prevState,
      username: value,
    }));
  };

  const handlerChangePassword = value => {
    setState(prevState => ({
      ...prevState,
      password: value,
    }));
  };

  const handlerOnEnter = e => {
    if (e.key === 'Enter') {
      handlerSubmit();
    }
  };

  const formError = () => {
    if (state.username.length === 0 || state.password.length === 0) return true;
    else return false;
  };

  return (
    <>
      <Alert message={msgError} setOpen={setOpen} open={open} type="error" />
      <Title>
        <Typography variant="h5" color="error">
          {type === 'signIn'
            ? i18n.formatMessage({ id: 'auth.title.login' })
            : i18n.formatMessage({ id: 'auth.title.register' })}
        </Typography>
      </Title>

      <form onKeyPress={handlerOnEnter}>
        <Grid container justify="center" spacing={2}>
          <Grid item xs={10}>
            <TextField
              className={classes.textField}
              inputProps={{ autoComplete: 'off', className: classes.input }}
              value={state.username}
              onChange={event => handlerChangeUsername(event.target.value)}
              autocomplete="off"
              placeholder={i18n.formatMessage({ id: 'auth.input.username.placeholder' })}
              color="secondary"
            />
          </Grid>
          <Grid item xs={10}>
            <TextField
              className={classes.textField}
              inputProps={{
                autoComplete: 'new-password',
                className: classes.input,
              }}
              value={state.password}
              onChange={event => handlerChangePassword(event.target.value)}
              type="password"
              placeholder={i18n.formatMessage({ id: 'auth.input.password.placeholder' })}
              color="secondary"
            />
          </Grid>
          <Grid item xs={10}>
            <WrapperButton>
              <DefaultButton
                onClick={handlerSubmit}
                disabled={formError()}
                data-testid="submit-btn"
              >
                {type === 'signIn'
                  ? i18n.formatMessage({ id: 'auth.login.button.submit' })
                  : i18n.formatMessage({ id: 'auth.register.button.submit' })}
              </DefaultButton>
            </WrapperButton>
          </Grid>
          <Grid item xs={10}>
            <Button
              data-testid="cancel-btn"
              color="primary"
              onClick={() => setType(type === 'signIn' ? 'signUp' : 'signIn')}
            >
              {type === 'signIn'
                ? i18n.formatMessage({ id: 'auth.register.button' })
                : i18n.formatMessage({ id: 'auth.login.button' })}
            </Button>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

export default AuthForm;
