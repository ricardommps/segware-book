import {
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_LOGOUT,
  AUTH_REGISTER_REQUEST,
  AUTH_REGISTER_SUCCESS,
  AUTH_REGISTER_FAILURE,
} from './authActions';

const initialState = {
  loginInFlight: false,
  loginError: false,
  token: sessionStorage.getItem('segware_book::token'),
  registerInFlight: false,
  registerError: false,
  registerSuccess: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case AUTH_LOGIN_REQUEST: {
      return {
        ...state,
        loginInFlight: true,
        loginError: false,
        registerInFlight: false,
        registerError: false,
        registerSuccess: false,
      };
    }
    case AUTH_LOGIN_SUCCESS: {
      return {
        ...state,
        token: payload.token,
        loginInFlight: false,
        loginError: false,
      };
    }
    case AUTH_LOGIN_FAILURE: {
      return { ...state, token: null, loginInFlight: false, loginError: true };
    }
    case AUTH_REGISTER_REQUEST: {
      return {
        ...state,
        registerInFlight: true,
        registerError: false,
        registerSuccess: false,
        loginError: false,
      };
    }
    case AUTH_REGISTER_SUCCESS: {
      return { ...state, registerInFlight: false, registerError: false, registerSuccess: true };
    }
    case AUTH_REGISTER_FAILURE: {
      return { ...state, registerInFlight: false, registerError: true, registerSuccess: false };
    }
    case AUTH_LOGOUT: {
      return { ...state, token: null, loginError: false };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
