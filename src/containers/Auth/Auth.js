import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import loginbackground from 'assests/loginbackground.png';
import AuthForm from './components/AuthForm';
import { login, register } from './authActions';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 2rem;
  background-color: #262729;
`;

const Auth = () => {
  const [state, setState] = useState({
    username: '',
    password: '',
  });
  const [type, setType] = useState('signIn');
  const dispatch = useDispatch();

  const handlerSubmit = () => {
    if (type === 'signIn') dispatch(login(state));
    else dispatch(register(state));
  };
  const loginError = useSelector(state => state.auth.loginError);
  const registerError = useSelector(state => state.auth.registerError);
  const registerSuccess = useSelector(state => state.auth.registerSuccess);

  return (
    <Grid container style={{ height: '100vh', backgroundColor: '#262729' }} data-testid="login">
      <Grid
        container
        justify="space-around"
        item
        xs={12}
        sm={9}
        style={{ backgroundColor: '#262729 ' }}
      >
        <Grid container justify="center" alignItems="center" item xs={12}>
          <img alt="Logo Tela" src={loginbackground} width="100%" height="100%" />
        </Grid>
      </Grid>
      <Grid container alignItems="center" item xs={12} sm={3}>
        <Wrapper>
          <AuthForm
            type={type}
            setType={setType}
            state={state}
            setState={setState}
            handlerSubmit={handlerSubmit}
            loginError={loginError}
            registerError={registerError}
            registerSuccess={registerSuccess}
          />
        </Wrapper>
      </Grid>
    </Grid>
  );
};

export default Auth;
