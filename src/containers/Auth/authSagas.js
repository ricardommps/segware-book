import { call, put, takeLatest, all } from 'redux-saga/effects';
import * as actions from './authActions';
import * as authService from './authService';

export function* loginSagas({ payload }) {
  try {
    console.log('--payload', payload);
    const { data } = yield call(authService.signIn, payload);
    sessionStorage.setItem('segware_book::token', data);
    yield put(actions.loginSuccess({ token: data }));
  } catch (e) {
    console.log('--', e);
    yield put(actions.loginFailure(e));
  }
}

export function* registerSagas({ payload }) {
  try {
    const { data } = yield call(authService.signUp, payload);
    yield put(actions.registerSuccess(data));
  } catch (e) {
    console.log('--', e);
    yield put(actions.registerFailure(e));
  }
}

export function logout() {
  console.log('----logout');
  sessionStorage.clear();
  window.location.reload();
}

const sagas = all([
  takeLatest(actions.AUTH_LOGIN_REQUEST, loginSagas),
  takeLatest(actions.AUTH_REGISTER_REQUEST, registerSagas),
  takeLatest('APP_RESET', logout),
]);

export default sagas;
