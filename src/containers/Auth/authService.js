import axios from 'axios';

export async function signIn({ username, password }) {
  return axios.post('api/sign-in', { username, password });
}

export async function signUp({ username, password }) {
  return axios.post('api/sign-up', { username, password });
}
