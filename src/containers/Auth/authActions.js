export const AUTH_LOGIN_REQUEST = 'AUTH_LOGIN_REQUEST';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_FAILURE = 'AUTH_LOGIN_FAILURE';

export const AUTH_REGISTER_REQUEST = 'AUTH_REGISTER_REQUEST';
export const AUTH_REGISTER_SUCCESS = 'AUTH_REGISTER_SUCCESS';
export const AUTH_REGISTER_FAILURE = 'AUTH_REGISTER_FAILURE';

export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const login = payload => ({
  type: AUTH_LOGIN_REQUEST,
  payload: payload,
});

export const loginSuccess = payload => ({
  type: AUTH_LOGIN_SUCCESS,
  payload: payload,
});

export const loginFailure = payload => ({
  type: AUTH_LOGIN_FAILURE,
  payload: payload,
});

export const register = payload => ({
  type: AUTH_REGISTER_REQUEST,
  payload: payload,
});

export const registerSuccess = payload => ({
  type: AUTH_REGISTER_SUCCESS,
  payload: payload,
});

export const registerFailure = payload => ({
  type: AUTH_REGISTER_FAILURE,
  payload: payload,
});

export const logout = payload => ({
  type: AUTH_LOGOUT,
  payload: payload,
});
