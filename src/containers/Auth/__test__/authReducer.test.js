import reducer from '../authReducer';
import * as actions from '../authActions';
import initialState from '../authReducer';
describe('Auth reducer', () => {
  it('should return the initial state', () => {
    expect(reducer({}, { type: null })).toEqual({});
  });
  describe('Sing-In', () => {
    it('should handle login', () => {
      expect(reducer(initialState, actions.login())).toEqual({
        ...initialState,
        loginInFlight: true,
        loginError: false,
        registerInFlight: false,
        registerError: false,
        registerSuccess: false,
      });
    });

    it('should handle loginSuccess', () => {
      const payload = {
        token: '',
      };
      expect(reducer(initialState, actions.loginSuccess(payload))).toEqual({
        ...initialState,
        token: payload.token,
        loginInFlight: false,
        loginError: false,
      });
    });
    it('should handle loginFailure', () => {
      const error = new Error('error');
      expect(reducer(initialState, actions.loginFailure(error))).toEqual({
        ...initialState,
        token: null,
        loginInFlight: false,
        loginError: true,
      });
    });
  });
  describe('Sing-Up', () => {
    const payload = {
      username: 'teste',
      password: '123',
    };
    it('should handle register', () => {
      expect(reducer(initialState, actions.register(payload))).toEqual({
        ...initialState,
        registerInFlight: true,
        registerError: false,
        registerSuccess: false,
        loginError: false,
      });
    });

    it('should handle registerSuccess', () => {
      expect(reducer(initialState, actions.registerSuccess())).toEqual({
        ...initialState,
        registerInFlight: false,
        registerError: false,
        registerSuccess: true,
      });
    });

    it('should handle registerFailure', () => {
      const error = new Error('error');
      expect(reducer(initialState, actions.registerFailure(error))).toEqual({
        ...initialState,
        registerInFlight: false,
        registerError: true,
        registerSuccess: false,
      });
    });
  });

  describe('Sing-Up', () => {
    it('should handle logout', () => {
      expect(reducer(initialState, actions.logout())).toEqual({
        ...initialState,
        token: null,
        loginError: false,
      });
    });
  });
});
