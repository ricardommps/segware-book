import axiosMock from 'axios';
import { recordSaga } from 'utils/test/recordSaga';
import { loginSagas, registerSagas } from '../authSagas';
import {
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_REGISTER_REQUEST,
  AUTH_REGISTER_SUCCESS,
  AUTH_REGISTER_FAILURE,
} from '../authActions';

jest.mock('axios');

const mockToken =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
describe('Auth Sagas', () => {
  describe('Sing-In sagas tests', () => {
    it('Success para login', async done => {
      const response = { data: mockToken };
      axiosMock.post.mockImplementationOnce(() => Promise.resolve(response));
      const result = await recordSaga(loginSagas, {
        type: AUTH_LOGIN_REQUEST,
        payload: {
          username: 'teste',
          password: '123',
        },
      });
      expect(result).toEqual([
        {
          type: AUTH_LOGIN_SUCCESS,
          payload: { token: mockToken },
        },
      ]);
      done();
    });

    it('Failure para login', async done => {
      const error = new Error('error');
      axiosMock.post.mockImplementationOnce(() => {
        throw error;
      });
      const result = await recordSaga(loginSagas, {
        type: AUTH_LOGIN_REQUEST,
        payload: {
          username: 'teste',
          password: '123',
        },
      });

      expect(result).toEqual([
        {
          type: AUTH_LOGIN_FAILURE,
          payload: error,
        },
      ]);
      done();
    });
  });

  describe('Sing-Up sagas tests', () => {
    it('Success para Register', async done => {
      const response = { data: {} };
      axiosMock.post.mockImplementationOnce(() => Promise.resolve(response));
      const result = await recordSaga(registerSagas, {
        type: AUTH_REGISTER_REQUEST,
        payload: {
          username: 'teste',
          password: '123',
        },
      });
      expect(result).toEqual([
        {
          type: AUTH_REGISTER_SUCCESS,
          payload: {},
        },
      ]);
      done();
    });

    it('Failure para login', async done => {
      const error = new Error('error');
      axiosMock.post.mockImplementationOnce(() => {
        throw error;
      });
      const result = await recordSaga(registerSagas, {
        type: AUTH_REGISTER_REQUEST,
        payload: {
          username: 'teste',
          password: '123',
        },
      });

      expect(result).toEqual([
        {
          type: AUTH_REGISTER_FAILURE,
          payload: error,
        },
      ]);
      done();
    });
  });
});
