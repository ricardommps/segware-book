import React from 'react';
import { Route } from 'react-router';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { createMemoryHistory } from 'history';
import { fireEvent, screen } from '@testing-library/react';
import Auth from '../Auth';
import { AppContext } from 'context/appContext';
import renderWithReactIntl, { reRenderWithReactIntl } from 'utils/test/RenderWithReactIntl';
import initialState from '../authReducer';

const createMockStore = state => ({
  auth: initialState,
  ...state,
});

let mockDispatch = jest.fn();
let mockStore = createMockStore();

jest.mock('react-redux', () => ({
  useDispatch: () => mockDispatch,
  useSelector: jest.fn(fn => fn(mockStore)),
}));

beforeEach(() => {
  mockStore = createMockStore();
  mockDispatch = jest.fn();
});

function renderWithRedux(history) {
  return renderWithReactIntl(
    <AppContext.Provider>
      <Router history={history}>
        <Route path="/">
          <Auth />
        </Route>
      </Router>
    </AppContext.Provider>
  );
}

describe('Auth component', () => {
  const history = createMemoryHistory();
  test('render componente Auth ', () => {
    const { getByTestId } = renderWithRedux(history);
    const element = getByTestId('login');
    expect(element).toBeInTheDocument();
  });
  test('login ', () => {
    const handlerSubmit = jest.fn();
    const { getByTestId, getByPlaceholderText } = renderWithRedux(history);
    const element = getByTestId('login');
    expect(element).toBeInTheDocument();
    const inputUsername = getByPlaceholderText('Username');
    expect(inputUsername.value).toBe(''); // empty before
    fireEvent.change(inputUsername, { target: { value: 'teste' } });
    expect(inputUsername.value).toBe('teste');
    const inputPassword = getByPlaceholderText('Password');
    expect(inputPassword.value).toBe(''); // empty before
    fireEvent.change(inputPassword, { target: { value: '123' } });
    expect(inputPassword.value).toBe('123');
    fireEvent.click(getByTestId('submit-btn'));
    expect(handlerSubmit).not.toHaveBeenCalled();
  });
});
