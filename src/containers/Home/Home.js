import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import { getFeeds, setReaction } from './homeActions';
import CardFeeds from 'components/CardFeed/Card';
import styled from 'styled-components';

const useStyles = makeStyles(theme => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  gridContainer: {
    overflow: 'auto',
    maxHeight: '100vh',
    '&:last-child': {
      paddingBottom: '100px',
    },
  },
}));

const Container = styled.div`
  height: 100%;
  width: 100%;
  padding: 30px;
`;

const Home = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const classes = useStyles();
  const { feeds } = useSelector(state => state.feeds);

  const handleReaction = (feedId, like, love) => {
    console.log('---handleReaction---', feedId, like, love);
    dispatch(setReaction(feedId, like, love));
  };

  useEffect(() => {
    dispatch(getFeeds());
  }, [dispatch]);
  return (
    <>
      <Container>
        <Grid container spacing={1} className={classes.gridContainer}>
          {feeds &&
            feeds.map(feed => (
              <Grid item xs={12} sm={4}>
                <CardFeeds feed={feed} handleReaction={handleReaction} />
              </Grid>
            ))}
        </Grid>
      </Container>
      <Fab
        color="primary"
        aria-label="add"
        className={classes.fab}
        onClick={() => history.push('/feed')}
      >
        <AddIcon />
      </Fab>
    </>
  );
};

export default Home;
