import axios from 'axios';
export async function feed(content) {
  return axios.post('api/feed', content);
}

export async function getFeeds() {
  return axios.get('api/feeds');
}

export async function reaction(reactions) {
  return axios.post('api/reaction', reactions);
}
