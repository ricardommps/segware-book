import { call, put, takeLatest, all } from 'redux-saga/effects';
import * as actions from './homeActions';
import * as homeService from './homeService';

function* getFeedsSagas() {
  try {
    const { data } = yield call(homeService.getFeeds);
    yield put(actions.getFeedsSuccess(data));
  } catch (e) {
    yield put(actions.getFeedsFailure(e));
  }
}

function* reactionSagas({ feedId, like, love }) {
  try {
    const data = { feedId: feedId, like: like, love: love };
    yield call(homeService.reaction, data);
    yield put(actions.setReactionSuccess());
    yield put(actions.getFeeds());
  } catch (e) {
    yield put(actions.setReactionFailure(e));
  }
}

export function* feedSaga({ payload }) {
  try {
    const data = { content: payload };
    console.log('----feedSaga--', data);
    yield call(homeService.feed, data);
    yield put(actions.setFeedSuccess());
  } catch (error) {
    yield put(actions.setFeedFailure(error));
  }
}

const sagas = all([
  takeLatest(actions.GET_FEEDS_REQUEST, getFeedsSagas),
  takeLatest(actions.SET_FEED_REQUEST, feedSaga),
  takeLatest(actions.REACTION_REQUEST, reactionSagas),
]);

export default sagas;
