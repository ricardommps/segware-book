import {
  GET_FEEDS_REQUEST,
  GET_FEEDS_SUCCESS,
  GET_FEEDS_FAILURE,
  SET_FEED_REQUEST,
  SET_FEED_SUCCESS,
  SET_FEED_FAILURE,
} from './homeActions';

const initialState = {
  feedsInFlight: false,
  feedsError: false,
  feeds: null,
  feedInFlight: false,
  feedError: false,
  feedSuccess: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_FEEDS_REQUEST: {
      return {
        ...state,
        feedsInFlight: true,
        feedsError: false,
        feeds: null,
      };
    }
    case GET_FEEDS_SUCCESS: {
      return {
        ...state,
        feedsInFlight: true,
        feedsError: false,
        feeds: payload,
      };
    }
    case GET_FEEDS_FAILURE: {
      return {
        ...state,
        feedsInFlight: true,
        feedsError: false,
        feeds: null,
      };
    }
    case SET_FEED_REQUEST: {
      return {
        ...state,
        feedInFlight: true,
        feedError: false,
        feedSuccess: false,
      };
    }
    case SET_FEED_SUCCESS: {
      return {
        ...state,
        feedInFlight: true,
        feedError: false,
        feedSuccess: true,
      };
    }
    case SET_FEED_FAILURE: {
      return {
        ...state,
        feedInFlight: true,
        feedError: false,
        feedSuccess: false,
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
