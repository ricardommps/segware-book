export const GET_FEEDS_REQUEST = 'GET_FEEDS_REQUEST';
export const GET_FEEDS_SUCCESS = 'GET_FEEDS_SUCCESS';
export const GET_FEEDS_FAILURE = 'GET_FEEDS_FAILURE';

export const SET_FEED_REQUEST = 'SET_FEED_REQUEST';
export const SET_FEED_SUCCESS = 'SET_FEED_SUCCESS';
export const SET_FEED_FAILURE = 'SET_FEED_FAILURE';

export const REACTION_REQUEST = 'REACTION_REQUEST';
export const REACTION_SUCCESS = 'REACTION_SUCCESS';
export const REACTION_FAILURE = 'REACTION_FAILURE';

export const getFeeds = () => ({
  type: GET_FEEDS_REQUEST,
});

export const getFeedsSuccess = data => ({
  type: GET_FEEDS_SUCCESS,
  payload: data,
});

export const getFeedsFailure = error => ({
  type: GET_FEEDS_FAILURE,
  payload: error,
});

export const setReaction = (feedId, like, love) => ({
  type: REACTION_REQUEST,
  feedId,
  like,
  love,
});

export const setReactionSuccess = () => ({
  type: REACTION_SUCCESS,
});

export const setReactionFailure = error => ({
  type: REACTION_FAILURE,
  payload: error,
});

export const setFeed = content => ({
  type: SET_FEED_REQUEST,
  payload: content,
});

export const setFeedSuccess = () => ({
  type: SET_FEED_SUCCESS,
});

export const setFeedFailure = error => ({
  type: SET_FEED_FAILURE,
  payload: error,
});
