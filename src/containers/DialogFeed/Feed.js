import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { setFeed, getFeeds } from 'containers/Home/homeActions';

const Feed = () => {
  const i18n = useIntl();
  const history = useHistory();
  const dispatch = useDispatch();
  const { feedSuccess } = useSelector(state => state.feeds);
  const [content, setContent] = useState('');
  const handleClose = () => {
    history.goBack();
  };
  const handleSubmit = () => {
    dispatch(setFeed(content));
  };
  useEffect(() => {
    if (feedSuccess) {
      history.push('/');
      dispatch(getFeeds());
    }
  }, [feedSuccess, history, dispatch]);
  return (
    <Dialog open={true} onClose={handleClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title"> {i18n.formatMessage({ id: 'feed.title' })}</DialogTitle>
      <DialogContent>
        <TextField
          id="content"
          label={i18n.formatMessage({ id: 'feed.input.label' })}
          multiline
          rows={4}
          variant="outlined"
          value={content}
          onChange={event => setContent(event.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          {i18n.formatMessage({ id: 'feed.button.cancel' })}
        </Button>
        <Button onClick={handleSubmit} color="primary" variant="contained">
          {i18n.formatMessage({ id: 'feed.button.submit' })}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default Feed;
