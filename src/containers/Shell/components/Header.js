import React from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  title: {
    flexGrow: 1,
  },
}));

const HeaderShell = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const logout = () => dispatch({ type: 'APP_RESET' });
  return (
    <header style={{ gridArea: 'header' }} data-testid="header">
      <AppBar color="secondary">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Segware Book
          </Typography>
          <Button color="inherit" onClick={logout}>
            Sair
          </Button>
        </Toolbar>
      </AppBar>
    </header>
  );
};
export default HeaderShell;
