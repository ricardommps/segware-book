import React, { memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Header from './components/Header';
import { AppContext } from 'context/appContext';

const useStyles = makeStyles(theme => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(7),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
}));

const Shell = ({ children }) => {
  const classes = useStyles();
  return (
    <AppContext.Provider>
      <Header />
      <main className={classes.content}>{children}</main>
    </AppContext.Provider>
  );
};
export default memo(Shell);
