import * as React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { messages, formats } from 'i18n';

const RenderWithReactIntl = (fragment, initialState) => {
  return render(
    <IntlProvider locale="pt-BR" timeZone="utc" messages={messages} formats={formats}>
      {fragment}
    </IntlProvider>,
    { ...initialState }
  );
};

export const reRenderWithReactIntl = (fragment, rerender) => {
  return {
    ...rerender(
      <IntlProvider locale="pt-BR" timeZone="utc" messages={messages} formats={formats}>
        {fragment}
      </IntlProvider>
    ),
  };
};

export default RenderWithReactIntl;
