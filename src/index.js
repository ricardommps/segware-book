import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Switch, Router } from 'react-router';
import axios from 'axios';
import { createBrowserHistory } from 'history';
import store from './store/store';
import App from './App';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Theme from 'theme/theme';
import './styles/app.css';

axios.defaults.baseURL = 'https://segware-book-api.segware.io/';
axios.interceptors.request.use(
  config => {
    const token = sessionStorage.getItem('segware_book::token');
    if (token) {
      config.headers['Authorization'] = 'Bearer ' + token;
    }
    // config.headers['Content-Type'] = 'application/json';
    return config;
  },
  error => {
    Promise.reject(error);
  }
);
export const history = createBrowserHistory();
ReactDOM.render(
  <MuiThemeProvider theme={Theme}>
    <Router history={history}>
      <Switch>
        <Provider store={store}>
          <App></App>
        </Provider>
      </Switch>
    </Router>
  </MuiThemeProvider>,
  document.getElementById('root')
);
