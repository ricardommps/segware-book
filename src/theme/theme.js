import { createMuiTheme } from '@material-ui/core/styles';
const Theme = createMuiTheme({
  typography: {
    fontFamily: `"Montserrat", Helvetica, Arial, sans-serif`,
    fontSize: 14,
  },
  palette: {
    primary: {
      main: '#3f51b5',
    },
    secondary: {
      main: 'rgba(0,0,0,0.8)',
    },
    error: {
      main: '#EF3340',
    },
  },
});

export default Theme;
