import { all } from 'redux-saga/effects';
import { AuthSagas } from 'containers/Auth';
import { HomeSagas } from 'containers/Home';

function* Saga() {
  yield all([AuthSagas, HomeSagas]);
}

export default Saga;
