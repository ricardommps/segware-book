import { combineReducers } from 'redux';
import { AuthReducer } from 'containers/Auth';
import { HomeReducer } from 'containers/Home';

const reducers = combineReducers({
  auth: AuthReducer,
  feeds: HomeReducer,
});

export default reducers;
